
// API APPLICATION PROGRAMMING INTERFACE
console.log("Hello World");
// Asynchronous Statement
// FETCH API - allows to asynchronously request for a ressource(data)
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));



fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log (response.status));

console.log("Goodbye");
// multiple .thens create a promise chain
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((json)=> console.log(json));

// ASYNC and AWAIT keywords -to indicates which portion should be waited for

async function fetchData(){
	// waits for the fetch method to complete then stores the value in the "result" variable
	let result= await fetch("https://jsonplaceholder.typicode.com/posts");
	// result returned by fetch is a returned promise
	console	.log(result);
	// the returned response is an object
	console.log(typeof result);
	// to access the content of res.,body property is used
	console.log(result.body);
	// convert data frm response to obect json
	let json= await result.json()
	console.log(json);

}

fetchData();
// NOTE: in API all of these is a compressed version of  GET method

// Getting a specific post---------------------------------
// getting post with user id 1
fetch("https://jsonplaceholder.typicode.com/posts/1").then((response)=> response.json()).then((json)=> console.log(json));

// CREATING a POST--------------------------------
/*SYNTAX:
	fetch('URL', options).then((response)=>{}).then((response)=>{});
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "New post",
		body: "Hello World",
		userID: 1
	})
}).then((response)=> response.json()).then((json)=> console.log(json));
// in postman add request post (POST)..paste the url, body,raw.json..the put values to the properties: title,body and userID then send.

// PUT METHOD-----------------------------------------
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method:"PUT",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again!",
		userID: 1
	})
}).then((response)=> response.json()).then((json)=> console.log(json));
// in postman add request post (PUT)..paste the url, body,raw.json..the put values to the properties: title,body and userID then send.

// PATCH METHOD UPDATE a post (updates a part object-----------------------------------------

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post"
	})
}).then((response)=> response.json()).then((json)=> console.log(json));

// in postman add request post (PATCH)..paste the url, with endpoint id, type the title value then send.




// DELETING POST-----------------------------------
// specify the post to delete using id
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});
// in postman add request post (DELETE)..paste the url with the endpoint id to delete, then send.